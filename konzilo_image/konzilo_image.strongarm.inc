<?php
/**
 * @file
 * konzilo_image.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function konzilo_image_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__konzilo_image';
  $strongarm->value = array(
    'view_modes' => array(
      'konzilo_image' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => TRUE,
      ),
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'featured' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'konzilo_promo' => array(
        'custom_settings' => FALSE,
      ),
      'revision' => array(
        'custom_settings' => FALSE,
      ),
      'konzilo_sidebar' => array(
        'custom_settings' => TRUE,
      ),
      'konzilo_slideshow' => array(
        'custom_settings' => FALSE,
      ),
      'konzilo_body_text' => array(
        'custom_settings' => FALSE,
      ),
      'konzilo_facts_box' => array(
        'custom_settings' => FALSE,
      ),
      'konzilo_image_main' => array(
        'custom_settings' => TRUE,
      ),
      'konzilo_image_author' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'metatags' => array(
          'weight' => '9',
        ),
        'title' => array(
          'weight' => '1',
        ),
        'path' => array(
          'weight' => '7',
        ),
        'xmlsitemap' => array(
          'weight' => '8',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__konzilo_image'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_konzilo_image';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_konzilo_image'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_konzilo_image';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_konzilo_image'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_konzilo_image';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_konzilo_image'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_konzilo_image';
  $strongarm->value = '1';
  $export['node_preview_konzilo_image'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_konzilo_image';
  $strongarm->value = 1;
  $export['node_submitted_konzilo_image'] = $strongarm;

  return $export;
}
