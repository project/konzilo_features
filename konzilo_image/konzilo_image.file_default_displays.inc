<?php
/**
 * @file
 * konzilo_image.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function konzilo_image_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__konzilo_image_main__file_field_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'konzilo_image_main',
    'image_link' => '',
  );
  $export['image__konzilo_image_main__file_field_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__konzilo_sidebar__file_field_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'konzilo_image_sidebar',
    'image_link' => '',
  );
  $export['image__konzilo_sidebar__file_field_image'] = $file_display;

  return $export;
}
