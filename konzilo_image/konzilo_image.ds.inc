<?php
/**
 * @file
 * konzilo_image.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function konzilo_image_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|konzilo_image|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'konzilo_image';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'field_konzilo_author' => array(
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'field_konzilo_body' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'description',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_konzilo_headline' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'headline',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
          'fi' => TRUE,
          'fi-el' => 'h1',
          'fi-cl' => 'title',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
      ),
    ),
  );
  $export['node|konzilo_image|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|konzilo_image|konzilo_image_main';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'konzilo_image';
  $ds_fieldsetting->view_mode = 'konzilo_image_main';
  $ds_fieldsetting->settings = array(
    'field_konzilo_file' => array(
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
  );
  $export['node|konzilo_image|konzilo_image_main'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|konzilo_image|konzilo_sidebar';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'konzilo_image';
  $ds_fieldsetting->view_mode = 'konzilo_sidebar';
  $ds_fieldsetting->settings = array(
    'field_konzilo_body' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'description',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_konzilo_file' => array(
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'field_konzilo_headline' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'headline',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
          'fi' => TRUE,
          'fi-el' => 'h2',
          'fi-cl' => 'title',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
      ),
    ),
  );
  $export['node|konzilo_image|konzilo_sidebar'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|konzilo_image|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'konzilo_image';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'field_konzilo_article_parts_node_bf' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link text' => '[node:title]',
        'limit' => '1',
        'wrapper' => 'h2',
        'class' => 'title',
        'ft' => array(),
      ),
    ),
    'field_konzilo_body' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'description',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_konzilo_file' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'image',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
  );
  $export['node|konzilo_image|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function konzilo_image_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|konzilo_image|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'konzilo_image';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_konzilo_file',
        1 => 'field_konzilo_headline',
        2 => 'field_konzilo_body',
        3 => 'field_konzilo_author',
      ),
    ),
    'fields' => array(
      'field_konzilo_file' => 'ds_content',
      'field_konzilo_headline' => 'ds_content',
      'field_konzilo_body' => 'ds_content',
      'field_konzilo_author' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|konzilo_image|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|konzilo_image|konzilo_image_main';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'konzilo_image';
  $ds_layout->view_mode = 'konzilo_image_main';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_konzilo_file',
        1 => 'field_konzilo_headline',
        2 => 'field_konzilo_body',
        3 => 'field_konzilo_author',
      ),
    ),
    'fields' => array(
      'field_konzilo_file' => 'ds_content',
      'field_konzilo_headline' => 'ds_content',
      'field_konzilo_body' => 'ds_content',
      'field_konzilo_author' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|konzilo_image|konzilo_image_main'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|konzilo_image|konzilo_sidebar';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'konzilo_image';
  $ds_layout->view_mode = 'konzilo_sidebar';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_konzilo_file',
        1 => 'field_konzilo_headline',
        2 => 'field_konzilo_body',
      ),
    ),
    'fields' => array(
      'field_konzilo_file' => 'ds_content',
      'field_konzilo_headline' => 'ds_content',
      'field_konzilo_body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|konzilo_image|konzilo_sidebar'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|konzilo_image|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'konzilo_image';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_konzilo_file',
        1 => 'field_konzilo_article_parts_node_bf',
        2 => 'field_konzilo_body',
      ),
    ),
    'fields' => array(
      'field_konzilo_file' => 'ds_content',
      'field_konzilo_article_parts_node_bf' => 'ds_content',
      'field_konzilo_body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|konzilo_image|teaser'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function konzilo_image_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'konzilo_image_main';
  $ds_view_mode->label = 'konzilo image main';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['konzilo_image_main'] = $ds_view_mode;

  return $export;
}
